from udp_inc import *
import sys 
import os


class udp:

   def read_reg(self):
        fp=os.path.abspath(os.path.dirname(__file__))
        file = open(fp+'/udp.reg', 'r')
        f = list(file);
        self.registry = dict();
        for a in f :
            b = a.split();
            self.registry[b[0]] = b[1];

   def __init__(self):
       self.read_reg();

   def dissect(self, buf,tree) :
       self.tree = tree; 
       level = self.tree["prev"] + 1;
       self.udp_src_port = [buf[0],buf[1]];
       self.udp_dst_port = [buf[2],buf[3]];
       t = dict();
       t["udp_src"] = self.udp_src_port;
       t["udp_dst"] = self.udp_dst_port;
       self.tree[level] = t;
       self.tree["prev"] = level;
       dp = (self.udp_dst_port[0] * 256) + self.udp_dst_port[1];
       dst_h = "{0:#0{1}x}".format(dp, 6)

       if (dst_h in self.registry.keys()):
            prot = self.registry[dst_h];
            print("protocol is", prot);
            x = getattr(sys.modules[__name__], prot) ; # class
            self.y = x(); # instance
            tr = self.y.dissect(buf[8:], self.tree);
            return(tr);
       else:
           t["payload"] = buf[8:];
           self.tree[level] = t;

       return (self.tree);

