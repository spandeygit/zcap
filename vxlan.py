from vxlan_inc import *
import sys 


class vxlan:

   def read_reg(self):
        file = open('vxlan.reg', 'r')
        f = list(file);
        self.registry = dict();
        for a in f :
            b = a.split();
            self.registry[b[0]] = b[1];

   def __init__(self):
       self.read_reg();

   def dissect(self, buf,tree) :
       self.tree = tree; 
       level = self.tree["prev"] + 1;
       self.vxlan_payload = buf[8:];
       t = dict();
       t["vxlan_pld"] = buf[8:]

       self.tree[level] = t;
       self.tree["prev"] = level;
       return (self.tree);

