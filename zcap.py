#Copyright (c) 2021 Shardendu Pandey

from  zcap_inc import *
import sys
import os

class zcap:
    def read_reg(self):
        file_path = os.path.abspath(os.path.dirname(__file__))
        file = open(file_path+'/zcap.reg', 'r')
        f = list(file);
        self.registry = dict();
        for a in f :
            b = a.split();
            self.registry[b[0]] = b[1];
        #print self.registry;


    def ascii2hex(self,v):
        if (sys.version_info >= (3, 0)):
                h = v;
        else :
                h = ord(v)

        l = int(h / 16);
        r = (h - (l * 16));
        return(self.pos2hex(l) + self.pos2hex(r))

    def pos2hex(self, v):
        if (v < 10) :
            return str(v);
        elif (v == 10):
            return "a";
        elif (v == 11):
            return "b";
        elif (v == 12):
            return "c";
        elif (v == 13):
            return "d";
        elif (v == 14):
            return "e";
        elif (v == 15):
            return "f";






    def get_word(self, w, magic):
        #print("MAGIC is ",magic);
        if (magic == "a1b2c3d4" or magic == "a1b23c4d"): # ns pcaps also encoded here 
            return(int(w,16));
        if (magic == "d4c3b2a1" or magic=="4d3cb2a1"):  # ns pcaps also encoded here 
            a = w[6:8] + w[4:6] + w[2:4] + w[0:2]
            return(int(a,16));
        return(-1)

    def pad0(self, string, length):
        a = list(string);
        b = ""
        if (len(a) < length):
            for i in range(length - len(a)):
                b = b + "0"
            b = b+string;
        else:
            b = string;
        return(b);

    # any length hex string to ascii string
    def hex2ascii(self, string):

        if (sys.version_info >= (3, 0)):
            a = list(string);
            ll = list(); r = ""
            for i in range(0,len(a),2):
                b = a[i] + a[i+1];
                x = int(b,16);
                ll.append(x);
            r = bytearray(ll)
        else:
            a = list(string);
            r = ""
            for i in range(0,len(a),2):
                b = a[i] + a[i+1];
                x = int(b,16);
                r  = r + chr(x);
        return(r);


    def write_file(self, name, fcson=0):
        self.wfile = name;
        f = open(self.wfile, 'wb')

        if (sys.version_info >= (3, 0)):
            f.write(self.hex2ascii("d4c3b2a1"));
            f.write(self.hex2ascii("0200"));
            f.write(self.hex2ascii("0400"));
            f.write(self.hex2ascii("00000000"));
            f.write(self.hex2ascii("00000000"));
            f.write(self.hex2ascii("ffff0000"));
            if (fcson) :
                f.write(self.hex2ascii("91000000")); # 4 bytes FCS
            else:
                f.write(self.hex2ascii("01000000")); # no FCS
        else :
            f.write(self.hex2ascii("d4c3b2a1"));
            f.write(self.hex2ascii("0200"));
            f.write(self.hex2ascii("0400"));
            f.write(self.hex2ascii("00000000"));
            f.write(self.hex2ascii("00000000"));
            f.write(self.hex2ascii("ffff0000"));
            if (fcson) :
                f.write(self.hex2ascii("91000000")); # 4 bytes FCS
            else:
                f.write(self.hex2ascii("01000000")); # no FCS

        return (f);

    # convert word to endian based characters
    def w2f(self, w, length, magic):
        b = w;
        #c = hex(b, length*2);
        c = "{0:#0{1}x}".format(b,length*2+2)
        c = c[2:]
        d = self.get_word(c,magic);
        e = hex(d); 
        e = e[2:]
        e = self.pad0(e,8) ; # pad 0 in beginning
        e = self.hex2ascii(e);
        return (e);

    def write_pkt(self, handle,pkt, magic):
        m,p = pkt;
        handle.write(self.w2f(m[0], 4, magic)); # ts1
        handle.write(self.w2f(m[1], 4, magic)); # ts2
        handle.write(self.w2f(m[2], 4, magic)); # CL 
        handle.write(self.w2f(m[3], 4, magic)); # OL 
        for i in range (len(p)):
            if (sys.version_info >= (3, 0)):
                handle.write(bytearray([p[i]])); # ts1
            else:
                handle.write(chr(p[i])); # ts1

    def close_file(self, handle):
        handle.close();


    def read_file(self, name):
        self.rfile = name;
        # read a data structure of meta and buffer for each packet 
        file = open(name, 'rb')
        f = list(file);
        retlist = list();
        state = "MAGIC";
        pkt = list();
        for a in f:
            pkt.extend(a);

        p = 0; n = 0;

        ##print (pkt)
        while (p < len(pkt)):
            # https://tools.ietf.org/id/draft-gharris-opsawg-pcap-00.html
            if (state == "MAGIC"):
                    self.magic = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    p = p+4;
                    state = "MAJOR";
            if (state == "MAJOR"):
                    self.major_ver = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) 
                    p = p+2;
                    state = "MINOR";
            if (state == "MINOR"):
                    self.minor_ver = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) 
                    p = p+2;
                    state = "RES1";
            if (state == "RES1"):
                    self.res1 = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    p = p+4;
                    state = "RES2";
            if (state == "RES2"):
                    self.res1 = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    p = p+4;
                    state = "SNAP";
            if (state == "SNAP"):
                    self.snap = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    p = p+4;
                    state = "LINK";
            if (state == "LINK"):
                    self.link = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    a = self.link;
                    b = list(a);
                    b = "0"+a[1:];
                    c = self.get_word(b,self.magic);
                    self.linktype = str(c);

                    p = p+4;
                    state = "HDR-TS1";
            if (state == "HDR-TS1"):
                    #print("TS1 At index %d"%(p));
                    self.hdr_ts1 = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    p = p+4;
                    state = "HDR-TS2";
            if (state == "HDR-TS2"):
                    #print("TS2 At index %d"%(p));
                    self.hdr_ts2 = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    p = p+4;
                    state = "HDR-CAPLEN";
            if (state == "HDR-CAPLEN"):
                    #print("CAPLEN At index %d"%(p));
                    self.hdr_caplen = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    cl = self.get_word(self.hdr_caplen, self.magic);
                    p = p+4;
                    #print ("Length is ",cl);
                    state = "HDR-ORIGLEN";
            if (state == "HDR-ORIGLEN"):
                    #print("HDR-OL At index %d"%(p));
                    self.hdr_origlen = self.ascii2hex(pkt[p]) + self.ascii2hex(pkt[p+1]) + self.ascii2hex(pkt[p+2]) + self.ascii2hex(pkt[p+3]) ;
                    l = self.get_word(self.hdr_origlen, self.magic);
                    p = p+4;
                    #print ("Length is ",l);
                    state = "PDATA";
            if (state == "PDATA"):
                    #print("PDATA At index %d"%(p));
                    #cl = self.get_word(self.hdr_caplen, self.magic);
                    self.pd = list();
                    for i in range (cl):
                        self.pd.append(int(self.ascii2hex(pkt[p]),16));
                        p = p+1;
                    thispkt = list();
                    thispkt = [[self.get_word(self.hdr_ts1, self.magic),\
                            self.get_word(self.hdr_ts2, self.magic), \
                            l, cl],self.pd];
                    retlist.append(thispkt);
                    n = n+1;
                    print("completed parsing packet ",n); 
                    state = "HDR-TS1";
                    #break;

        #print ("MAGIC  - ",self.magic)
        #print ("MAJORV - ",self.major_ver)
        #print ("MINORV - ",self.minor_ver)
        #print ("SNAP - ",self.snap)
        #print ("LINK - ",self.link)
        #print ("RETLIST - ", retlist);
        return (retlist);

    def init_dissect(self, buf, linktype):
        # parser 
        prot = self.registry[linktype];
        x = getattr(sys.modules[__name__], prot) ; # class https://stackoverflow.com/questions/4821104/dynamic-instantiation-from-string-name-of-a-class-in-dynamically-imported-module
        self.y = x(); # instance
        self.prot_tree[0] = "pcap";
        self.prot_tree["prev"] = 0;
        t = self.y.dissect(buf,self.prot_tree);
        return (t);

    def print_pkt(self, ts, p):
        l = len(p); k = 0;
        #print("time = %f Length = %d",ts,l);
        line = 0;
        print("%05d : %s"%(line,ts));
        while (k < l):
            n = 0; j = ""
            while (n <8) :
                if ((n+k) < l):
                    j = j + "%02x "%(p[k+n])
                n = n+1
            k = k+8;
            line = line+1;
            print("%05d : %s"%(line,j));
        print("...............................");




    def __init__(self):
        self.prot_tree = dict();
        self.read_reg();


