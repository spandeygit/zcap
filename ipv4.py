from ipv4_inc import *
import sys 
import os


class ipv4:

   def read_reg(self):
        fp = os.path.abspath(os.path.dirname(__file__))
        file = open(fp+'/ipv4.reg', 'r')
        f = list(file);
        self.registry = dict();
        for a in f :
            b = a.split();
            self.registry[b[0]] = b[1];

   def __init__(self):
       self.read_reg();

   def dissect(self, buf,tree) :
       self.tree = tree; 
       level = self.tree["prev"] + 1;
       self.ipv4_dest = [buf[16],buf[17],buf[18],buf[19]];
       self.ipv4_src = [buf[12],buf[13],buf[14],buf[15]];
       self.ipv4_proto = buf[9];
       t = dict();
       t["proto"] = "ipv4";
       t["ipv4sa"] = self.ipv4_src;
       t["ipv4da"] = self.ipv4_dest;
       t["ipv4proto"] = self.ipv4_proto;
       self.tree[level] = t;
       self.tree["prev"] = level;
       proto_h = "{0:#0{1}x}".format(self.ipv4_proto,4)
       rep = 20;

       if (proto_h in self.registry.keys()):
            prot = self.registry[proto_h];
            x = getattr(sys.modules[__name__], prot) ; # class
            self.y = x(); # instance
            tr = self.y.dissect(buf[rep:], self.tree);
            return(tr);




       return (self.tree);

