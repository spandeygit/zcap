from zcap import *
import zlib;
import sys

def listToString(s): 
    str1 = "" 
    for ele in s: 
        str1 += chr(ele)  
    if (sys.version_info > (3, 0)):
        str1 = str1.encode('utf-8');

    return str1


# open foo pcap --> delete duplicates 
# then duplicate all packets that are unique 

x = zcap();
r = x.read_file("foo_new.pcap");

for m,p in r:
    tr = x.init_dissect(p,x.linktype);
    #print (tr);
    x.print_pkt(m,p);

w = x.write_file("foo_dedup.pcap",0);
dup = dict();
uniq = 0;
for e in r:
    crc = zlib.crc32(listToString(e[1]));
    if (crc in dup.keys()):
        print ("crc found duplicate %x"%(crc));
        pass;
    else :
        print ("crc unique %x"%(crc));
        dup[crc] = 1;
        uniq +=1;
        x.write_pkt(w,e,"d4c3b2a1");

print ("unique packets = %d"%(uniq));
x.close_file(w);


