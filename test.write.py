import sys 

if (sys.version_info >= (3, 0)):
    # python3 considers strings as UNICODE 
    st = bytearray([0xd4,0xc3,0xb2,0xa1]);    # WORKS  :) 
    #st = '\xd4\xc3\xb2\xa1'.encode('UTF-8'); # FAILS  :(
    print(type(st))
    o = open("test.txt", "wb")
    o.write(st)
    o.close()
else :
    # python2 considers string naturally as ASCII 
    st = '\xd4\xc3\xb2\xa1'
    print(st);
    print(type(st))
    o = open("test.txt", "wb")
    o.write(st)
    o.close()

