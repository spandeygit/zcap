from eth_inc import *
import sys 
import os

class eth:



   def read_reg(self):
        fp = os.path.abspath(os.path.dirname(__file__))
        file = open(fp+'/eth.reg', 'r')
        f = list(file);
        self.registry = dict();
        for a in f :
            b = a.split();
            self.registry[b[0]] = b[1];

   def __init__(self):
       self.read_reg();


   #------------------------
   # MAC + MPLS supported 
   #------------------------

   def dissect(self, buf, tree) :
       self.tree = tree; 
       level = self.tree["prev"] + 1;
       self.mac_dest = [buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]];
       self.mac_src = [buf[6],buf[7],buf[8],buf[9],buf[10],buf[11]];
       self.mac_eth = [buf[12],buf[13]];
       t = dict();
       t["proto"] = "eth";
       t["macsa"] = self.mac_src;
       t["macda"] = self.mac_dest;
       t["maceth"] = self.mac_eth;

       rep = 14; bot = 0;
       if (self.mac_eth == [0x88,0x47]) :
           rep = 14; bot = 0;
           self.mplsstack = list();
           while (bot ==0):
                self.mplsstack.append([buf[rep], buf[rep+1], buf[rep+2], buf[rep+3]]);
                bot = buf[rep+2] & 0x01;
                rep = rep+4;

       # now detect if following is L3 
       if ((buf[rep] & 0xf0 ) == 0x60) :
            self.mac_eth = [0x86, 0xdd];
       if ((buf[rep] & 0xf0 ) == 0x40) :
            self.mac_eth = [0x08, 0x00];



       self.tree[level] = t;
       self.tree["prev"] = level;
       etht = (self.mac_eth[0] * 256) + self.mac_eth[1];
       etht_h = "{0:#0{1}x}".format(etht,6)
       if (etht_h in self.registry.keys()):
            prot = self.registry[etht_h];
            x = getattr(sys.modules[__name__], prot) ; # class
            self.y = x(); # instance
            tr = self.y.dissect(buf[rep:], self.tree);
            return(tr);

       return(self.tree);




